# README #

Asus T100HA is a Cherry Trail based 2-in-1 device which is not currently supported by any Linux distribution out of the box.
This repository is going to contain all the files or scripts that are needed to get the basic functionality online on that device.

Please create separate branches for all alternative config files or scripts that you believe will improve the OS stability on T100HA/Cherry Trail.

All suggestions, ideas or questions should be sent to info-box@null.net

### Who do I talk to? ###

Repository owner: Paweł Wilk | vlku@null.net